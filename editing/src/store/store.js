import { createStore} from "redux";
import itemsReducer from "./items-reducer.js";


const store = createStore(itemsReducer);

export default store;