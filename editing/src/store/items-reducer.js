import { nanoid } from "nanoid";
import itemActionTypes from "./item-action-types.js";

const initialState = {
    items: [
        {id: nanoid(), name: "test1"}, 
        {id: nanoid(), name: "test2"}, 
        {id: nanoid(), name: "test3"}
    ],
    state: "state",
    editingItem: null,
    filterState:""
}

const itemsReducer = (state = initialState, action) => {

    switch (action.type) {
        
        case itemActionTypes.ADD_ITEM: 
            const newId = nanoid();
            const newItem = {id: newId, ...action.payload};
            return {...state, items:[...state.items, newItem]};

        case itemActionTypes.REMOVE_ITEM:
            const itemId = action.payload;
            return {...state, items: [...state.items.filter(item => item.id !== itemId)]};

        case itemActionTypes.START_EDIT_ITEM:
            const editId = action.payload;
            return {...state, editingItem: [...state.items.filter(item => item.id === editId)][0]};

        case itemActionTypes.FINISH_EDIT_ITEM:
            const {id} = action.payload;
            const filteredItems = [...state.items.filter(item => item.id !== id)];
            return {...state, items: [...filteredItems, action.payload], editingItem: null};

        case itemActionTypes.CANCEL_EDIT_ITEM:
            return {...state, editingItem: null};

        case itemActionTypes.FILTER_ITEM:
            return {...state, filterState: action.payload};
            
        default: return state;
    }
    
}


export default itemsReducer;