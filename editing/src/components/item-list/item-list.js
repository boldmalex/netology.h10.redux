import react from "react";
import { useSelector, useDispatch } from "react-redux";
import itemActionTypes from "../../store/item-action-types";
import "./item-list.css";


const ItemList = () => {

    const dispatch = useDispatch();

    const items = useSelector((store) => store.items);
    const editingItem = useSelector((store) => store.editingItem);
    const filterState = useSelector((store) => store.filterState);


    const handleRemove = (id) => {
        dispatch({type: itemActionTypes.REMOVE_ITEM, payload: id});
    };

    
    const handleChange = (id) => {
        dispatch({type: itemActionTypes.START_EDIT_ITEM, payload: id});
    };


    const getItems = () => {
        if (filterState && filterState.length > 0){
            return items.filter(item => item.name.toLowerCase().includes(filterState.toLowerCase()));
        }
        else
            return items;
    }


    const showItem = (item) => {
        return(
            <li key={item.id}>
                    {item.name} 
                    
                    <button onClick={() => handleRemove(item.id)}
                            className={editingItem && item.id === editingItem.id ? "btn-disabled" : ""}>
                        X
                    </button>

                    <button onClick={() => handleChange(item.id)} 
                            className={editingItem && item.id === editingItem.id ? "btn-disabled" : ""}>
                        edit
                    </button>
            </li>
        );
    }


    if (items) 
        return (
            <ul>
                {getItems().map(item => showItem(item))}
            </ul>
        );
    else
        return null;
}

export default ItemList;