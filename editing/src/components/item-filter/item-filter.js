import React from "react";
import { useDispatch } from "react-redux";
import itemActionTypes from "../../store/item-action-types";
import "./item-filter.css";

const ItemFilter = () => {
    
    const dispatch = useDispatch();

    const handleChange = (evt) => {
        evt.preventDefault();
        dispatch({type: itemActionTypes.FILTER_ITEM, payload: evt.target.value})
    } 

    return (
        <div>
            <input type="text" className="filter" onChange={handleChange} placeholder="set filter"></input>
        </div>
    )
}

export default ItemFilter;