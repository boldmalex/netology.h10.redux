import react, { useEffect, useState } from "react";
import itemActionTypes from "../../store/item-action-types";

import { useSelector, useDispatch } from "react-redux";

const ItemAddEdit = () => {

    const dispatch = useDispatch();

    const editingItem = useSelector((store) => store.editingItem);
    
    const [name, setName] = useState(editingItem ? editingItem.name : '');


    useEffect(() => {
        editingItem && setName(editingItem.name);
    }, [editingItem])


    const handleChange = (evt) => {
        evt.preventDefault();
        setName(evt.target.value);
    }
    

    const handleSubmit = (evt) => {
        evt.preventDefault();

        if (editingItem !== null)
            dispatch({type: itemActionTypes.FINISH_EDIT_ITEM, payload: {...editingItem, name: name}}) 
        else
            dispatch({type: itemActionTypes.ADD_ITEM, payload: {name}})
        
        setName('');
    }

    const handleCancel = (evt) => {
        evt.preventDefault();
        dispatch({type: itemActionTypes.CANCEL_EDIT_ITEM}) 
        setName('');
    }

    const showCancelButton = () =>{
        return (
            editingItem && <button onClick={handleCancel}>Cancel</button>
        )
    }

    
    return (
        <form onSubmit={handleSubmit}>
            <input  type="text" 
                    name="name"
                    value={name}
                    onChange={handleChange}>
            </input>
            <button type="submit">Save</button>
            {showCancelButton()}
        </form>
        
    )

}

export default ItemAddEdit;