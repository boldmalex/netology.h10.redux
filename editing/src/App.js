import react from "react";
import {Provider} from 'react-redux';
import ItemAddEdit from "./components/Item-add/item-add-edit";
import ItemFilter from "./components/item-filter/item-filter";
import ItemList from "./components/item-list/item-list";
import store from "./store/store";


function App() {
  return (
    <Provider store={store}>
        <ItemFilter/>
        <ItemAddEdit/>
        <ItemList/>
    </Provider>
  );
}

export default App;
